class NetworkCoordinatorsController < ApplicationController

  def manage_fogl2_request
    # => This is a global controller to manage request to the external system ...
    
    house_id = params[:house_id].to_i
    external_system_base_url = params[:external_system_base_url]
    method_type = params[:method_type]
    route_path = params[:route_path]
    encrypted_payload = params[:encrypted_payload]
    message_digest = params[:message_digest]

    puts "\n\n coming berroe herw"

    respond_to do |format|
      format.json {
        puts "\n\n coming inside format.json - #{![:post, :get].include?(method_type)}"
        if external_system_base_url.nil? || encrypted_payload.nil? || ![:post, :get, "post", "get"].include?(method_type)
          render json: {
            :message => "Invalid Request from Fog Layer 2",
            :status => 401
          } and return
        end
        
        puts "\n by passed the nil check in controller"

        payload_from_agent = {
          house_id: house_id,
          encrypted_payload: encrypted_payload,
          message_digest: message_digest,
          method_type: method_type,
          route_path: route_path,
          external_system_base_url: external_system_base_url
        }

        Rails.logger.info "payload_from_agent in controller - #{payload_from_agent}"

        FogNetworkManager.send_information_to_external_system(house_id, external_system_base_url, method_type, route_path, encrypted_payload, message_digest)
      }
    end  
  end

end