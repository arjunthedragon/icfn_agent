class FogNetworkManager

  class << self

    def send_information_to_external_system(house_id, external_system_base_url = nil, method_type = nil, route_path = nil, encrypted_payload = {}, message_digest = nil)
      # => This is a generalized method to send information to the external network or system ...
      
      puts "\n agent arguments - #{house_id}, #{external_system_base_url}, #{method_type}, #{route_path}, #{encrypted_payload}, #{message_digest}"
      
      return if external_system_base_url.nil?
      return unless [:post, :get, "post", "get"].include?(method_type)

      fog_api = FogApi.new(external_system_base_url)

      payload_from_agent = {
        house_id: house_id,
        encrypted_payload: encrypted_payload,
        message_digest: message_digest
      }

      Rails.logger.info "payload_from_agent - #{payload_from_agent}"

      if method_type == :post || method_type == "post"
        response = fog_api.conn.post route_path, payload_from_agent.to_json
      end
    end


    


    handle_asynchronously :send_information_to_external_system
  end

end