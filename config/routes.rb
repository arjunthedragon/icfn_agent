Rails.application.routes.draw do
  
  root to: 'static_pages#home'

  
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html 

  resources :network_coordinators, only: [] do
    collection do
      post :manage_fogl2_request
    end
  end

end