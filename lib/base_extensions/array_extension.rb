module ArrayExtension

  extend ActiveSupport::Concern

  # add your instance methods here

  def has_duplicate?
    !(self.count == self.uniq.count)
  end
  
  
  def average
    arr = self.compact
    avg = ((arr.inject{ |sum, ele| sum + ele }.to_f) / arr.size)
    (avg.nan?) ? nil : avg
  end
  
  
  def has_any_array_elements?(array)
    (self && array).any?
  end


  def has_any_element_equal_to_number?(number)
    return false if number.nil?

    self.compact.any?{|ele| ele == number }
  end


  def has_any_element_greater_than_number?(number)
    return false if number.nil?

    self.compact.any?{|ele| ele > number }
  end


  def has_any_element_greater_than_or_equal_to_number?(number)
    return false if number.nil?

    self.compact.any?{|ele| ele >= number }
  end

 
  def has_any_element_less_than_number?(number)
    return false if number.nil?

    self.compact.any?{|ele| ele < number }
  end

 
  def has_any_element_less_than_or_equal_number?(number)
    return false if number.nil?

    self.compact.any?{|ele| ele <= number }
  end
end

# include the extension 
Array.send(:include, ArrayExtension)